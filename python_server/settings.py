"""
Django settings for python_server project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from unipath import Path
import json
import os


BASE_DIR = Path(__file__).absolute().ancestor(2)
# BASE_DIR = os.path.dirname(os.path.dirname(__file__))


try:
    with open(BASE_DIR.child('secrets.json')) as handle:
        SECRETS = json.load(handle)
except IOError:
    SECRETS = {}


DEBUG = SECRETS.get('debug', False)
TEMPLATE_DEBUG = SECRETS.get('debug', False)

ALLOWED_HOSTS = SECRETS.get('allowed_hosts', [])

SERVER = str(SECRETS.get('server', None))

# Database Settings
if not SECRETS.get('engine') or SECRETS.get('engine') == 'sqlite3':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }
elif SECRETS.get('engine') == 'mysql':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': SECRETS.get('db_name', 'dbname'),
            'USER': SECRETS.get('db_user', 'root'),
            'PASSWORD': SECRETS.get('db_password', 'password'),
            'HOST': SECRETS.get('db_host', '127.0.0.1'),
        }
    }
SECRET_KEY = SECRETS.get('secret_key', 'm)5)of34!)5xx#8&k6zfk2t5e(kqp1_y$ox81%uq=et!$@ig#$')


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
#SECRET_KEY = '^#@z2zmgt3z(gdh90s)!s)6c)w&!-&638hoa(f+%^wzz91sdez'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'grader',
    # 'login',
    # 'social.apps.django_app.default',
)

LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_FACEBOOK_KEY = "280525545474859"
SOCIAL_AUTH_FACEBOOK_SECRET = "02c633de25ea2580277bf16e657fb4d6"

TEMPLATE_CONTEXT_PROCESSORS = (
   'django.contrib.auth.context_processors.auth',
   'django.core.context_processors.debug',
   'django.core.context_processors.i18n',
   'django.core.context_processors.media',
   'django.core.context_processors.static',
   'django.core.context_processors.tz',
   'django.contrib.messages.context_processors.messages',
   # 'social.apps.django_app.context_processors.backends',
   # 'social.apps.django_app.context_processors.login_redirect',
)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

AUTHENTICATION_BACKENDS = (
#    'social.backends.facebook.FacebookOAuth2',
#    'social.backends.google.GoogleOAuth2',
#    'social.backends.twitter.TwitterOAuth',
    'django.contrib.auth.backends.ModelBackend',
)

ROOT_URLCONF = 'python_server.urls'

WSGI_APPLICATION = 'python_server.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
