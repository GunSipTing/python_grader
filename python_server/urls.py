from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

	url(r'^admin/', include(admin.site.urls)),
    
    #url('^testSubmit/$', 'grader.views.test_submit', name='test_submit'),
    url('^submit/$', 'grader.views.submit', name='submit'),

    url(r'^$', 'grader.views.index', name='home'),
    url(r'^problem/(?P<id>[0-9]+)/$', 'grader.views.problem', name='problem'),

    url('^submission/$', 'grader.views.submission_index', name='submission_index'),
    url('^user/(?P<name>.+)/$', 'grader.views.user_submission', name='user_submission'),
    url(r'^submission/(?P<id>[0-9]+)/$', 'grader.views.submission', name='submission'),


    # url(r'^login/', include('login.urls')),
)
