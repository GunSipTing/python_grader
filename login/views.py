from django.shortcuts import render_to_response
from django.template.context import RequestContext

def home(request):
   context = RequestContext(request,
                           {'request': request,
                            'user': request.user})

   #print request.user.social_auth.get(provider='facebook').uid,"TTTTT"
   #request.user.get_full_name()
   #print request.user
   return render_to_response('thirdauth/home.html',
                             context_instance=context)

def detail(request):
   context = RequestContext(request,
                           {'request': request,
                            'user': request.user})

   #request.user.get_full_name()
   print request.user.social_auth.get(provider='facebook').extra_data
   return render_to_response('thirdauth/detail.html',
                             context_instance=context)