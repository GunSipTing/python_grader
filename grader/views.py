from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from django.forms.models import modelformset_factory
from grader.models import *
from grader.grade import *
from time import time, clock
from datetime import datetime

# Create your views here.

def truncate(A):
    return [ i[:75] + (i[75:] and '...') for i in A]


def index(request):


    l = Problem.objects.all()
    context = {'l': l}
    return render(request, 'index.html', context)
    

def _index(request):

    data = {}

    problems = list(Problem.objects.all())
    results = []

    code = ["A","B","C"]

    for _ in xrange(len(problems)):
        i = problems[_]

        results.append((i.name,i.typee,i.description,i.checker,i.time_limit,[]))
         
        print i.name+">>"
                
        t = GradeProblem(i,code[_]).result()
        
        print t[0],"<<<"
        print t[1]
        print "-"*25
        for j in TestCase.objects.filter(problem__exact = i):

            results[-1][-1].append(GradeTest(i,j).result(code[_]))

    data["result"] = results



    return render(request,'test.html',data)



def submit(request):
    data = {}

    if request.method == 'POST':
        problem = Problem.objects.get(id=request.POST['problem_id'])
        submit_file = request.FILES['submit_file']
        submit_file.name = str(datetime.now()).replace(' ', '_').replace(":","") + '.cpp';
        verdict = 'In Queue'
        description = 'Nothing Here'

        user =  request.POST['name']
        if not user:
            raise Exception("Please Input User");
        print "USER :",user
        submission = Submission(problem=problem, input_file=submit_file, verdict=verdict, description=description , user = user)
        submission.save()
        result_sub = GradeProblem(problem,submit_file.name,submission).result()


        submission.verdict,submission.description  = result_sub
        submission.save()

        data["verdict"] = submission.verdict
        data["description"] = submission.description
        data["sid"] = submission.id
        data["problem"] = submission.problem.name
        data["user"] = submission.user

        #print "<br/>".join(submission.description.split("\n"))
    return render(request, 'result.html', data)

# @login_required(login_url='/login/')
def problem(request,id):
    problem = Problem.objects.filter(id=id)[0]


    data = { 'p': problem }#, 'user':request.user }
    return render(request, 'problem.html', data)



def submission_index(request):
    l = Submission.objects.order_by('-id')[:50]
    context = {'l': l}
    return render(request, 'submission_index.html', context)
    

def user_submission(request,name):
    l = Submission.objects.filter(user=name).order_by('-id')[:50]
    context = {'l': l}
    return render(request, 'submission_index.html', context)


def submission(request,id):
    submission = Submission.objects.filter(id=id)[0]
    data = {}

    data["verdict"] = submission.verdict
    data["description"] = submission.description
    data["sid"] = submission.id
    data["problem"] = submission.problem.name
    data["user"] = submission.user

    return render(request, 'result.html', data)


