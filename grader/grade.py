from subprocess import Popen, PIPE, STDOUT , check_call ,call
import threading
from unipath import Path
from grader.models import *
from time import time,clock

def truncate(i):
    return i[:75] + (i[75:] and '...')


PATH = Path(__file__).absolute().ancestor(2).child("temp")

class GradeProblem(threading.Thread):
    def __init__(self,problem,file_name,submission):
        threading.Thread.__init__(self)
        self.P = problem
        self.file_name = file_name
        self.status = "In Queue"
        self.des = ""
        self.true = True
        self.sub = submission
        self.time = 0

    def run(self):
        self.status = ""

        
        testCase = list(TestCase.objects.filter(problem__exact = self.P))


        L = len(testCase)
        self.status = ["?"]* L

        for i in xrange(L):
            test = testCase[i]
            index = i+1
            
            #self.status = "Running on test#"+str(index)
            #print self.status
            result = GradeTest(self.P,test).result(self.file_name)
            self.time += float(result.time)
            self.des += "\n"+"-"*50+"\n"
            self.des += "Test #"+str(index)+"\n"
            self.des += "\n".join(map(lambda (x,y):x+":\n"+y+"\n",result.to_list()))


            self.status = self.status
            if result.status is not "Correct":
                #self.status = result.status + " on test#"+str(index)

                if result.status == "TIMEOUT":
                    self.status[i] = "T"
                elif result.status == "ERROR":
                    self.status[i] = "X"
                else:
                    self.status[i] = "_"
                self.true = False
                #break
            else:
                self.status[i] = "P"

            self.sub.verdict = "".join(self.status)
            print "%5d : %s" %(self.sub.id,self.sub.verdict)
            self.sub.save()

            

        if self.true:
            pass
            #self.status = "Accept"
        self.des = ("Overall Time : %.3f s \n" % self.time) + self.des

        self.status = "".join(self.status)

    def result(self):
        self.start()
        self.join()
        return self.status,self.des

#GradeTest(Problem,Test).result(file_name)
class Proc(threading.Thread):
    def __init__(self,file_name,str_in,timeout):

        threading.Thread.__init__(self)
        
        self.timeout = timeout
        self.file_name = file_name
        self.str_in = str_in
        self.result = "TIMEOUT"
        self.process = None
        self.second = 0

    def run(self):  
           
        #print args 
        def target():
            try:        
                """
                #python
                args = ["python", PATH.child(self.file_name) ]
                p = Popen( args, stdout=PIPE, stdin=PIPE, stderr=PIPE)
                self.result = p.communicate(self.str_in)
                print self.result
                """

            
                check_call(["g++", "-o" , PATH.child(self.file_name)+".out" , PATH.child(self.file_name ) ])

                self.seconds = clock()
                self.process = Popen( [PATH.child(self.file_name)+".out"], stdout=PIPE, stdin=PIPE, stderr=PIPE)
                self.result = self.process.communicate(self.str_in)

                #print map(truncate,self.result)
                self.seconds = clock() - self.seconds 
                
                if self.result[1] != '': 
                    raise Exception(self.result[1])



            except Exception as e:
                print str(e).strip()
                self.result = "ERROR"

        thread = threading.Thread(target=target)
        thread.start()

        thread.join(self.timeout)
        if thread.is_alive():
            self.process.terminate()
            thread.join()



class GradeTest:
    def __init__(self,problem,testcase):
        self.P = problem
        self.T = testcase

    def result(self,file_name):

        Input = self.T.inputt
        Expect = str(self.T.expect).strip().splitlines()
        Expect = "\n".join(Expect)
        
        t = Proc(file_name, Input ,self.P.time_limit)
        t.start()
        t.join(self.P.time_limit)
        seconds = t.seconds
        
        Output = ""
        Status = ""
        #print t.result#,"Here<<<<<<<<<<"
        if t.result in ("TIMEOUT","ERROR"):
            Status = t.result
            if t.result == "TIMEOUT":
                seconds = self.P.time_limit

        else:
            Output = t.result[0].strip().splitlines()
            Output = "\n".join(Output)  

            Status = Output==Expect
            
            Status = ["Wrong","Correct"][Status]
        seconds = "%.3f" % seconds
        Input,Output,Expect = map(truncate,(Input,Output,Expect))
        Input = Input.strip()

        return result(input = Input, output = Output, expect = Expect, time = seconds, status = Status)

class result:
    def __init__(self,input,output,expect,time,status):
        self.input = input
        self.output = output
        self.expect = expect
        self.time = time
        self.status = status
    def to_str(self):
        return "%s %s %s %s %s"%(self.input,self.output,self.expect,self.time,self.status)

    def to_list(self):
        return [("input",self.input),
                ("output",self.output),
                ("expect",self.expect),
                ("time",self.time),
                ("status",self.status)]

