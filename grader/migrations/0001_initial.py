# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Problem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField()),
                ('author', models.CharField(max_length=50)),
                ('time_limit', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('input_file', models.FileField(upload_to=b'temp')),
                ('author', models.CharField(max_length=50)),
                ('sent', models.DateTimeField(auto_now=True)),
                ('verdict', models.CharField(max_length=50)),
                ('description', models.TextField()),
                ('problem', models.ForeignKey(to='grader.Problem')),
            ],
        ),
        migrations.CreateModel(
            name='TestCase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('inputt', models.TextField()),
                ('expect', models.TextField(default=b'-', blank=True)),
                ('problem', models.ForeignKey(to='grader.Problem')),
            ],
        ),
    ]
