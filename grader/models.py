# -*- coding: utf-8 -*-

from django.db import models

# Create your models here.

def truncate(x):
    return x[:75] + (x[75:] and '...')

class Problem(models.Model):
    
    name = models.CharField(max_length=50)
    description = models.TextField()
    #checker = models.TextField(default="-",blank=True)
    #typee = models.CharField(max_length=20) 
    author = models.CharField(max_length=50)
    time_limit = models.FloatField()

    def __unicode__(self):
        return "%s"%(self.name)
    
class TestCase(models.Model):

    problem = models.ForeignKey('Problem')
    inputt = models.TextField()
    expect = models.TextField(blank=True,default="-")
    
    
    def __unicode__(self):

        return "%s : %s : %s"%(self.problem.name,truncate(self.inputt),truncate(self.expect))

class Submission(models.Model):
    problem = models.ForeignKey('Problem')
    input_file = models.FileField(upload_to='temp')
    user = models.CharField(max_length=50 , blank=False)
    sent = models.DateTimeField(auto_now=True)
    verdict = models.CharField(max_length=50)
    description = models.TextField()
    
    def __unicode__(self):
        return "Submission %s at %s" % (self.problem.name, self.sent)
    
    def __str__(self):
        return "%4d : %s : %s" % (self.id,self.problem.name,self.verdict)

from django.forms import ModelForm

class SubmissionFrom(ModelForm):
    class Meta:
        model = Submission
        fields = ['problem', 'input_file', 'user']

