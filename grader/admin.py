from django.contrib import admin
from grader.models import *

admin.site.register(Problem)
admin.site.register(TestCase)
admin.site.register(Submission)